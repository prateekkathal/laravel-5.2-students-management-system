@extends('layouts.master')

@section('title', 'List of Students')

@section('content')

  <div class="row">

    <?php if(!empty($studentData)) { ?>
      <div class="col-md-4">
      </div>

      <div class="col-md-4">
        <div>
          <b>Full Name:</b><br/><?php echo (!empty($studentData['full_name'])) ? $studentData['full_name'] : ''; ?>
        </div>

        <br/>

        <div>
          <b>Residence Address:</b><br/><?php echo (!empty($studentData['address'])) ? $studentData['address'] : ''; ?>
        </div>

        <br/>

        <div>
          <b>Gender:</b><br/><?php echo (!empty($studentData['gender'])&&$studentData['gender'] == 'm') ? 'Male' : 'Female'; ?>
        </div>

        <br/>

        <div>
          <b>Expected Year Of Passing:</b><br/><?php echo $studentData['year_of_passing']; ?>
        </div>

        <br/>

        <div>
          <b>Extra Curricular Activities:</b><br/>
          <?php foreach($studentData->interests as $interest) { ?>
          <?php echo ucfirst($interest->name).'<br/>'; ?>
          <?php } ?>
        </div>

        <br/>
        
        <a href="<?php echo url('/students/'.$studentData['id'].'/edit'); ?>">Edit</a> | <a href="<?php echo url('/students'); ?>">Go Back</a>
      </div>

      <div class="col-md-4">
      </div>
    <?php } ?>
    </div>
  </div>

@endsection