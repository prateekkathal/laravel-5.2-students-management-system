@extends('layouts.master')

@section('title', 'Add Student Record')

@section('content')

  <div class="row">

    <div class="col-md-4">
    </div>

    <div class="col-md-4">
      <form action="{{ route('createStudent') }}" method="POST">
        {!! csrf_field() !!}
        
        <?php $isValidated = !empty(old());?>

        <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
          <b>Full Name</b>
          <input type="text" class="form-control" maxlength="50" pattern="[a-zA-Z ]+" name="full_name" <?php echo ($isValidated) ? 'value="'.old('full_name').'"' : ''; ?> required>
            @if ($errors->has('full_name'))
              <span class="help-block">
                  <strong>{{ $errors->first('full_name') }}</strong>
              </span>
            @endif
        </div>

        <br/>

        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
          <b>Residence Address</b>
          <textarea maxlength="100" cols="50" rows="4" class="form-control" name="address" required><?php echo ($isValidated) ? old('address') : ''; ?></textarea>
          @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
          @endif
        </div>

        <br/>
        
        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
          <b>Gender</b> <br/>
          <input type="radio" name="gender" value="m" <?php echo ($isValidated)? ((old('gender') == 'm')? 'checked': '') : ''; ?> required> Male <br/>
          <input type="radio" name="gender" value="f" <?php echo ($isValidated)? ((old('gender') == 'f')? 'checked': '') : ''; ?> required> Female<br/>
          @if ($errors->has('gender'))
            <span class="help-block">
                <strong>{{ $errors->first('gender') }}</strong>
            </span>
          @endif
        </div>

        <br/>
        
        <div class="form-group{{ $errors->has('year_of_passing') ? ' has-error' : '' }}">
          <b>Expected Year Of Passing</b><br/>
          <select name="year_of_passing" required>
            <option value="">Select Year</option>
            <option value="2016" <?php echo ($isValidated) ? ((old('year_of_passing') == '2016') ? 'selected': '') : ''; ?>>2016</option>
            <option value="2017" <?php echo ($isValidated) ? ((old('year_of_passing') == '2017') ? 'selected': '') : ''; ?>>2017</option>
            <option value="2018" <?php echo ($isValidated) ? ((old('year_of_passing') == '2018') ? 'selected': '') : ''; ?>>2018</option>
            <option value="2019" <?php echo ($isValidated) ? ((old('year_of_passing') == '2019') ? 'selected': '') : ''; ?>>2019</option>
            <option value="2020" <?php echo ($isValidated) ? ((old('year_of_passing') == '2020') ? 'selected': '') : ''; ?>>2020</option>
          </select>
          @if ($errors->has('year_of_passing'))
            <span class="help-block">
                <strong>{{ $errors->first('year_of_passing') }}</strong>
            </span>
          @endif
        </div>

        <br/>
        
        <div class="form-group{{ $errors->has('interests') ? ' has-error' : '' }}">
          <b>Extra Curricular Activities</b><br/>
          @foreach ($interestsList as $interest)
            <div class="checkbox">
                <input type="checkbox" name="interests[]" value="{{ $interest->name }}" <?php echo (!empty(old('interests')))&&in_array($interest->name,old('interests')) ? 'checked' : ''; ?>><?php echo ucfirst($interest->name); ?>
            </div>
          @endforeach
        </div>

        <br/>
        
        <input class="btn btn-sm" type="submit" value="Add"> | <a href="<?php echo url('/students'); ?>">Cancel</a>
      </form>
    </div>

    <div class="col-md-4">
    </div>
  
  </div>
    
@endsection