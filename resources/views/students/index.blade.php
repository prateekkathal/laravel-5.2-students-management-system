@extends('layouts.master')

@section('title', 'List of Students')

@section('content')

	<div class="row">
		<div class="col-md-2">
		</div>

		<div class="col-md-8">

			<div class="alert alert-info text-center" id="alert-info-1" style="display:none;"></div>
			@if (Session::has('message'))
				<div class="alert alert-info text-center" id="alert-info-2">{{ Session::get('message') }}</div>
			@endif
			
			<?php if(!empty($students)) {
				foreach($students as $student) { ?>
					<div id="student-<?php echo $student['id']; ?>">
						<div class="col-md-9">
							<h4><a href="<?php echo url('students/'.$student['id']); ?>" class="studentLink"><?php echo $student['id']; ?>. <?php echo $student['full_name']; ?></a></h4>
						</div>
						<div class="col-md-3 edit_delete_buttons">
							<a href="<?php echo url('/students/'.$student['id'].'/edit'); ?>">Edit</a> | 
							<a href="javascript:void(0);" onclick="deleteStudent(<?php echo $student['id']; ?>)">Delete</a>
							<div id="error-<?php echo $student['id']; ?>"></div>
						</div>
						<div style="clear:both;"></div>
					</div>
				<?php
				}
			} 
			?>
			<br/>
			
			<div align="right"> {{ $students->links() }} </div>

			<br/>

			<center>
				<a href="<?php echo url('students/new'); ?>" class="btn btn-lg">Add Student</a>
			</center>
		</div>

		<div class="col-md-2">
		</div>
	</div>

@endsection