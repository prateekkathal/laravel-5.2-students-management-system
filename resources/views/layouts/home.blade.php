<!DOCTYPE html>
<html>
  <head>
    <title>@yield('title') | BTE Engineering College</title>
    <!-- CSS -->
    <link href="{{ URL::asset('flat-startup/css/theme.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('flat-startup/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('flat-startup/css/animate.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
    
    <!-- JS -->
    <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('flat-startup/js/bootstrap.js') }}"></script>
    <script src="{{ URL::asset('flat-startup/js/theme.js') }}"></script>
    <script src="{{ URL::asset('flat-startup/js/jquery.zoom-scroller.js') }}"></script>

  </head>
  <body>

    <div role="navigation" class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo url(''); ?>" class="navbar-brand">BTE Engineering College</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <?php if(!empty($user)) { ?>
              <li><a href="#">Welcome, <?php echo $user->name; ?></a></li>
              <li><a href="<?php echo url('students/new'); ?>">Add Students</a></li>
              <li><a href="<?php echo url('students'); ?>">List all Students</a></li>
              <li><a href="<?php echo url('logout'); ?>">Logout</a></li>
            <?php } else { ?>
              <li><a href="<?php echo url('login'); ?>">Log In</a></li>
              <li><a href="<?php echo url('register'); ?>">Register</a></li>
            <?php } ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div><!--/.container -->
    </div>

  <div class="jumbotron" id="top">
      <div class="fus-bg"></div>
      <div class="fus-overlay"></div>
      <div class="container">
        <div class="fus-pitch">
          <div class="fus-call-to-action">
            <h1>BTE Engineering College</h1>
            <h2>Students Management Website</h2>
            <div class="fus-btns">
              <a class="scrollto btn fus-btn-white" href="#feature">Learn More</a>
              <a class="btn btn-primary" href="<?php echo url('register'); ?>">Sign Up</a>
            </div>
          </div>
        </div>
      </div>
    </div>

<div class="fus-section fus-feature fus-white-bg" id="feature">
      <div class="container">
        <div class="fus-section-header">
          <h3>Join Now!</h3>
          <h4>API, Offline mode, iPhone and Android apps, etc.</h4>
        </div>

        <div class="tab-content fus-tab-content">
          <div class="tab-pane active">
            <div class="row">
              <div class="col-md-6"> 
                <p style="font-size:16px;">
                  <ul>
                  <li>Single software to manage all School / college related information.</li>
                  <li>Reliable and secure</li>
                  <li>Complete Automation of operations</li>
                  <li>Multiple college / school management</li>
                  <li>One-time cost to purchase the software</li>
                  <li>Creation of the college's / school's tech savvy image</li>
                  <li>Easy performance monitoring of individual modules leading to uncomplicated error detection</li>
                  <li>Centralized data repository for trouble-free data access</li>
                  <li>Elimination of people-dependent processes</li>
                  <li>User friendly interface requiring minimal learning and IT skills</li>
                  </ul>
                </p>
              </div>
              <div class="col-md-6">
                <p>
                  <center>
                  <a class="btn btn-lg" href="<?php echo url('login'); ?>">Log In</a>
                  <br/><br/><br/>
                  <a class="btn btn-lg" href="<?php echo url('register'); ?>">Sign Up</a>
                  </center>
                </p>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>

  </body>
</html>
