<!DOCTYPE html>
<html>
  <head>
    <title>@yield('title') | BTE Engineering College</title>
    <!-- CSS -->
    <link href="{{ URL::asset('flat-startup/css/theme.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('flat-startup/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('flat-startup/css/animate.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
    
    <!-- JS -->
    <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('flat-startup/js/bootstrap.js') }}"></script>
    <script src="{{ URL::asset('flat-startup/js/theme.js') }}"></script>
    <script src="{{ URL::asset('flat-startup/js/jquery.zoom-scroller.js') }}"></script>
    <script src="{{ URL::asset('js/main.js') }}"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}" />

  </head>
  <body>
 
    <div role="navigation" class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo url(''); ?>" class="navbar-brand">BTE Engineering College</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <?php if(!empty($user)) { ?>
              <li><a href="#">Welcome, <?php echo $user->name; ?></a></li>
              <li><a href="<?php echo url('students/new'); ?>">Add Students</a></li>
              <li><a href="<?php echo url('students'); ?>">List all Students</a></li>
              <li><a href="<?php echo url('logout'); ?>">Logout</a></li>
            <?php } else { ?>
              <li><a href="<?php echo url('login'); ?>">Log In</a></li>
              <li><a href="<?php echo url('register'); ?>">Register</a></li>
            <?php } ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div><!--/.container -->
    </div>

    <div class="fus-section fus-blog" id="blog">
      <div class="container">
        <div class="fus-section-header" style="margin-top:50px;">
          <h3>@yield('title')</h3>
        </div>

        <div class="row">
          <div class="col-md-12">
              @yield('content')
          </div>
        </div>

      </div>
    </div>

  </body>
</html>