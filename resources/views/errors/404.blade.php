@extends('layouts.master')

@section('title', '404: Page Not Found')

@section('content')

	<div class="row">
		<div class="col-md-2">
		</div>

		<div class="col-md-8">
			<center>
				<h3>The page you are looking for doesn't exist.</h3>
				<br/>
				<a href="<?php echo url(''); ?>">Go To Home</a>
			</center>
		</div>

		<div class="col-md-2">
		</div>
	</div>

@endsection