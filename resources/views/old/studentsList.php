<!DOCTYPE html>
<html>
	<head>
		<title>List of Students | BTE Engineering College</title>
		<script type="text/javascript">
			function deleteStudent(studentId) {
				if(studentId !== undefined) {
					var ans = confirm("Are you sure you want to delete this record?");
					if(ans == true) {
						location.href = "<?php echo url('/deleteStudents/'); ?>" + "/" + studentId;
					}
				}
			}
		</script>
	</head>
	<body style="margin-left:20px;">
		<div style="float:left;">
			<h1>BTE Engineering College</h1>
		</div>
		<div style="float:left;margin:30px;">
			<a href="<?php echo url('addStudents'); ?>">Add Students</a> | <a href="<?php echo url(''); ?>">List all Students</a>
		</div>
		<div style="clear:both;"></div>
		<h2>All Students</h2>
		<?php if(!empty($students)) {
			$total_students = count($students);
			for($i=0;$i<$total_students;$i++) {
			echo '<div style="float:left;width:33%;"><h2>'.$students[$i]['id'].'. '.$students[$i]['full_name'].'</h2></div><div style="float:left;margin-top:25px;"><a href="'.url('/editStudents/'.$students[$i]['id']).'">Edit Student<a/> | <a href="javascript:void(0);" onclick="deleteStudent('.$students[$i]['id'].')">Delete Student</a></div><div style="clear:both;"></div>';
			}
		} 
		?>
	</body>
</html>