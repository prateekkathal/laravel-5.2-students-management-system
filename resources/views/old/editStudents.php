@extends('layouts.master')

@section('title', 'Edit Student Record')

@section('content')

	<div class="row">
		
		<div class="col-md-4">
    </div>
		
		<div class="col-md-4">
			<form action="<?php echo url('students/'.$studentId); ?>" method="POST">
				<input type="hidden" name="_method" value="PUT">
	    	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

				<b>Full Name</b><br/>
				<input type="text" size="50" class="form-control" maxlength="50" pattern="[a-zA-Z ]+" name="full_name" required <?php echo (!empty($full_name)) ? 'value="'.$full_name.'"' : ''; ?>><br/><br/>
				
				<b>Residence Address</b><br/>
				<textarea maxlength="100" cols="50" rows="4" class="form-control" name="address" required><?php echo (!empty($address)) ? $address : ''; ?></textarea><br/>

				<br/>
				
				<b>Gender</b><br/>
				<input type="radio" name="gender" value="m" <?php echo (!empty($gender)&&$gender == 'm') ? 'checked' : ''; ?> required> Male <br/>
				<input type="radio" name="gender" value="f" <?php echo (!empty($gender)&&$gender == 'f') ? 'checked' : ''; ?> required> Female<br/>
				
				<br/>
				
				<b>Expected Year Of Passing</b><br/>
				<select name="year_of_passing">
					<option value="">Select Year</option>
					<option value="2016" <?php echo (!empty($year_of_passing)&&$year_of_passing == '2016') ? 'selected' : ''; ?>>2016</option>
					<option value="2017" <?php echo (!empty($year_of_passing)&&$year_of_passing == '2017') ? 'selected' : ''; ?>>2017</option>
					<option value="2018" <?php echo (!empty($year_of_passing)&&$year_of_passing == '2018') ? 'selected' : ''; ?>>2018</option>
					<option value="2019" <?php echo (!empty($year_of_passing)&&$year_of_passing == '2019') ? 'selected' : ''; ?>>2019</option>
					<option value="2020" <?php echo (!empty($year_of_passing)&&$year_of_passing == '2020') ? 'selected' : ''; ?>>2020</option>
				</select><br/>

				<br/>
				
				<b>Extra Curricular Activities</b><br/>
				<?php $totalInterests = count($interestsList);
				for($i=0;$i<$totalInterests;$i++) { ?>
				<input type="checkbox" name="extra_curr[]" value="<?php echo $interestsList[$i]; ?>" <?php echo (!empty($extra_curr)&&in_array($interestsList[$i],$extra_curr)) ? 'checked' : ''; ?>> <?php echo ucfirst($interestsList[$i]); ?> <br/>
				<?php } ?>
				
				<br/>

				<input class="btn btn-sm" type="submit" value="Edit"> | <a href="<?php echo url(''); ?>">Cancel</a>
			</form>
		</div>

		<div class="col-md-4">
		</div>

	</div>

@endsection