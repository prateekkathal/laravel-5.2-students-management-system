@extends('layouts.master')

@section('title', 'Add Student Record')

@section('content')

	<?php echo (!empty($error)) ? '<font color="red">'.$error.'</font><br/><br/>' : '<br/>'; ?>

	<div class="row">
		<div class="col-md-4">
    </div>
		<div class="col-md-4">
			<form action="<?php echo url('students'); ?>" method="POST">
				<b>Full Name</b><br/>
				<input type="text" class="form-control" maxlength="50" pattern="[a-zA-Z ]+" name="full_name" class="form-control" required <?php echo (!empty($full_name)) ? 'value="'.$full_name.'"' : ''; ?>><br/><br/>
				
				<b>Residence Address</b><br/>
				<textarea maxlength="100" cols="50" rows="4" class="form-control" name="address" class="form-control" required><?php echo (!empty($address)) ? $address : ''; ?></textarea><br/><br/>
				
				<b>Gender</b><br/>
				<input type="radio" name="gender" value="m" <?php echo (!empty($gender)&&$gender == 'm') ? 'checked' : ''; ?> required> Male <br/>
				<input type="radio" name="gender" value="f" <?php echo (!empty($gender)&&$gender == 'f') ? 'checked' : ''; ?> required> Female<br/><br/>
				
				<b>Expected Year Of Passing</b><br/>
				<select class="form-control" name="year_of_passing" required>
					<option value="" required>Select Year</option>
					<option value="2016" <?php echo (!empty($year_of_passing)&&$year_of_passing == '2016') ? 'selected' : ''; ?>>2016</option>
					<option value="2017" <?php echo (!empty($year_of_passing)&&$year_of_passing == '2017') ? 'selected' : ''; ?>>2017</option>
					<option value="2018" <?php echo (!empty($year_of_passing)&&$year_of_passing == '2018') ? 'selected' : ''; ?>>2018</option>
					<option value="2019" <?php echo (!empty($year_of_passing)&&$year_of_passing == '2019') ? 'selected' : ''; ?>>2019</option>
					<option value="2020" <?php echo (!empty($year_of_passing)&&$year_of_passing == '2020') ? 'selected' : ''; ?>>2020</option>
				</select><br/><br/>
				
				<b>Extra Curricular Activities</b><br/>
				<input type="checkbox" name="extra_curr[]" value="sports" <?php echo (!empty($extra_curr)&&in_array('sports',$extra_curr)) ? 'checked' : ''; ?>> Sports <br/>
				<input type="checkbox" name="extra_curr[]" value="programming" <?php echo (!empty($extra_curr)&&in_array('programming',$extra_curr)) ? 'checked' : ''; ?>> Programming <br/>
				<input type="checkbox" name="extra_curr[]" value="arts" <?php echo (!empty($extra_curr)&&in_array('arts',$extra_curr)) ? 'checked' : ''; ?>> Arts <br/>
				<input type="checkbox" name="extra_curr[]" value="music" <?php echo (!empty($extra_curr)&&in_array('music',$extra_curr)) ? 'checked' : ''; ?>> Music <br/><br/>
				
				<input class="btn btn-sm" type="submit" value="Add"> | <a href="<?php echo url(''); ?>">Cancel</a>
			</form>
		</div>
		<div class="col-md-4">
		</div>
	</div>
		
@endsection