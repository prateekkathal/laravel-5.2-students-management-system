<?php 

namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Students;
use App\Models\Interests;

class MainController extends Controller {

  /**
   * Show the profile for the given user.
   *
   * @param  int  $id
   * @return Response
   */
  public function index(Request $request) {
  	$data = array( 'user' => loggedInUserDetails() );
    return view('home', $data);
  }

}

?>