<?php 

namespace App\Http\Controllers;

use Validator, Auth, Request, Session;
use App\Http\Controllers\Controller;
use App\Models\Students;
use App\Models\Interests;

class StudentsController extends Controller {

  /**
   * Show the profile for the given user.
   *
   * @param  int  $id
   * @return Response
   */

  public function index() {
      $students = Students::paginate(10);
      $data = array('user' => loggedInUserDetails(), 'students' => $students);
      return view('students/index',$data);
  }

  public function _new(Request $request) {
      $data = array('user' => loggedInUserDetails(), 'interestsList' => Interests::getAllInterests());
      return view('students/new',$data);
  }

  public function create(Request $request) {
      $student = Students::insert(Request::all());
      if(!empty($student)&&empty($student['route'])) {
        Session::flash('message', "New student '" . $student->full_name . "' added successfully. " );
      } else {
        return redirect()->route($student['route'])->withErrors($student['validator'])->withInput();
      }
      return redirect()->route('studentsPage');
  }

  public function show(Request $request, $studentId = NULL) {
    $data = array('user' => loggedInUserDetails());
    if(!empty($studentId)) {
      $studentData = Students::getById($studentId);
      if(!empty($studentData)) {
        $studentData['interestsList'] = Interests::getAllInterests();
        $data['studentData'] = $studentData;
        return view('students/show', $data);
      } else {
      return redirect()->route('studentsPage');
      }
    } else {
      return redirect()->route('studentsPage');
    }
  }

  public function edit(Request $request, $studentId = NULL) {
    $data = array('user' => loggedInUserDetails());
    if(!empty($studentId)) {
      $studentData = Students::getById($studentId);
      if(!empty($studentData)) {
        $studentData['interestsList'] = Interests::getAllInterests();
        $data['studentData'] = $studentData;
        return view('students/edit', $data);
      } else {
        return redirect()->route('studentsPage');
      }
    } else {
      return redirect()->route('studentsPage');
    }
  }

  public function update(Request $request, $studentId = NULL) {
    if(!empty($studentId)) {
      $student = Students::updateById($studentId, Request::all());
      if(!empty($student)&&empty($student['route'])) {
        Session::flash('message', "Student '" . $student->full_name . "' updated successfully. " );
        return redirect()->route('studentsPage');
      } else {
        return redirect()->route($student['route'], array('id' => $student['id']))->withErrors($student['validator'])->withInput();
      }
    } else {
      return redirect()->route('studentsPage');
    }
  }

  public function delete(Request $request, $studentId = NULL) {
      $students = Students::deleteById($studentId);
  }

}

?>