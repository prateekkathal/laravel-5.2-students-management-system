<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
	Main Controller

Route::group(['middleware' => 'web', 'auth'], function() {
	Route::get('/', 'MainController@indexPage')->name('homePage');
});
*/

/*
	Users Controller
*/

/*
	Students Controller
*/
Route::group(['middleware' => 'web'], function() {
	Route::auth();
	
	Route::get('/', 'MainController@index')->name('homePage');
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web','auth']], function () {
	Route::get('/students', 'StudentsController@index')->name('studentsPage');
	Route::get('/students/new', 'StudentsController@_new')->name('newStudent');
	Route::post('/students', 'StudentsController@create')->name('createStudent');
	Route::get('/students/{id}', 'StudentsController@show')->where( 'id', '[0-9]+' )->name('infoStudent');
	Route::get('/students/{id}/edit', 'StudentsController@edit')->where( 'id', '[0-9]+' )->name('editStudent');
	Route::put('/students/{id}', 'StudentsController@update')->where( 'id', '[0-9]+' )->name('updateStudent');
	Route::delete('/students/{id}', 'StudentsController@delete')->where( 'id', '[0-9]+' )->name('deleteStudent');
});
