<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interests extends Model
{
  public $timestamps = false;
  protected $fillable = ['students_id','interests_id'];

	public function students() {
	    return $this->belongsToMany('App\Models\Students');
	}

	public static function getAllInterests() {
    $interestsList = array();
    $allInterests = Interests::all();
    return $allInterests;
  }

}

?>