<?php

namespace App\Models;

use DB, Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Students extends Model
{
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['full_name','address','gender','year_of_passing'];
	
	public function interests() {
		return $this->belongsToMany('App\Models\Interests')->whereNull('interests_students.deleted_at');
  }

  public static function insert($studentData) {
    $interests = new Interests();
    $validator = Validator::make($studentData, [
      'full_name' => 'required|max:50',
      'address' => 'required',
      'gender' => 'required',
      'year_of_passing' => 'required'
    ]);
    if($validator->fails()) {
      return array('route'=>'newStudent', 'validator'=> $validator); 
    } else {
      $studentDetails = Students::create($studentData);
      if(!empty($studentData['interests']) && !empty($studentDetails)) {
        foreach($studentData['interests'] as $interest) {
          $interestDetails = $interests->where('name', '=', $interest)->first();
          $studentsInterests = $studentDetails->interests()->attach($interestDetails->id);
        }
      }
      return $studentDetails;
    }
  }

  public static function getById($studentId) {
    try {
      $studentDetails = Students::with('interests')->find($studentId);
      if(!empty($studentDetails)) {
        return $studentDetails;
      } else {
        return FALSE;
      }
    } catch (Exception $e) {
      return FALSE;
    }
  }

  public static function updateById($studentId, $studentData) {
    $interests = new Interests();
    $studentDetails = Students::getById($studentId);
    if($studentDetails) { 
      $validator = Validator::make($studentData, [
        'full_name' => 'required|max:50',
        'address' => 'required',
        'gender' => 'required',
        'year_of_passing' => 'required'
      ]);
      if($validator->fails()) {
        return array('route' => 'editStudent', 'id' => $studentId, 'validator' => $validator); 
      } else {
        $updated = $studentDetails->update($studentData);
        $interestsIds = array();
        if(!empty($studentData['interests'])&&!empty($updated)) {
          foreach($studentData['interests'] as $interest) {
            $interestDetails = $interests->where('name', '=', $interest)->first();
            array_push($interestsIds, $interestDetails->id);
          }
        }
        $studentDetails->interests()->sync($interestsIds);
        return $studentDetails;
      }
    } else {
      return FALSE;
    }
  }

  public static function deleteById($studentId) {
      $studentData = Students::getById($studentId);
      //$studentData->interests()->detach();
      foreach($studentData->interests as $interest) {
        DB::table('interests_students')->where('students_id', $studentData->id)->where('interests_id', $interest->id)->update(array('deleted_at' => DB::raw('NOW()')));
      }
      $studentData->delete();
  }
}

?>