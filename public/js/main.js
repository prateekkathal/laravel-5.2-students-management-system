	var websiteUrl = 'http://localhost:8000/'

	$(document).ready(function() {

		$.ajaxSetup({
		  headers: {
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		  timeout: 3000
		});

	});

	function deleteStudent(studentId) {
		if(studentId !== undefined) {
			var ans = confirm("Are you sure you want to delete this record?");
			if(ans == true) {
				$.ajax ({
					url: (websiteUrl + 'students/' + studentId ),
					type: 'POST',
					data: {
						'_method': 'DELETE', 
						'_token': $('meta[name="csrf-token"]').attr('content')
					},
					success: function (result) {
						$('#student-' + studentId).hide();
            $('#alert-info-1').text('Record Deleted!');	
            $('#alert-info-1').show();  
            $('#alert-info-2').hide();
					},
					failure: function (result) {
						$('#student-' + studentId).text('Unable to delete!');
					}
				});
			}
		}
	}